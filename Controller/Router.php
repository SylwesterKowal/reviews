<?php
declare(strict_types=1);

namespace Kowal\Reviews\Controller;

use Magento\Framework\App\Action\Forward;
use Magento\Framework\App\ActionFactory;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\RouterInterface;

/**
 * Class Router
 */
class Router implements RouterInterface
{
    /**
     * @var ActionFactory
     */
    private $actionFactory;

    /**
     * @var ResponseInterface
     */
    private $response;

    /**
     * Router constructor.
     * @param ActionFactory $actionFactory
     * @param ResponseInterface $response
     * @param \Kowal\Reviews\Helper\Data $dataHelper
     */
    public function __construct(
        ActionFactory $actionFactory,
        ResponseInterface $response,
        \Kowal\Reviews\Helper\Data $dataHelper
    ) {
        $this->actionFactory = $actionFactory;
        $this->response = $response;
        $this->dataHelper = $dataHelper;
    }

    /**
     * @param RequestInterface $request
     * @return ActionInterface|null
     */
    public function match(RequestInterface $request): ?ActionInterface
    {
        $identifier = trim($request->getPathInfo(), '/');

        $url = $this->dataHelper->getConfig('url_opini') ?? 'opinie';

        if (strpos($identifier, $url) !== false) {
            $request->setModuleName('reviews');
            $request->setControllerName('all');
            $request->setActionName('index');
//            $request->setParams([
//                'first_param' => 'first_value',
//                'second_param' => 'second_value'
//            ]);

            return $this->actionFactory->create(Forward::class, ['request' => $request]);
        }

        return null;
    }
}
