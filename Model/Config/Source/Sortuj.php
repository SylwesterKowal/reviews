<?php
/**
 * Display All Reviews
 * Copyright (C) 2019  kowal sp. z o.o.
 *
 * This file is part of Kowal/Reviews.
 *
 * Kowal/Reviews is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Kowal\Reviews\Model\Config\Source;

class Sortuj implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        return [['value' => 'created_at', 'label' => __('Date')],['value' => 'title', 'label' => __('Title')],['value' => 'customer_id', 'label' => __('Customer')],['value' => 'entity_pk_value', 'label' => __('Product')]];
    }

    public function toArray()
    {
        return ['created_at' => __('Date'),'title' => __('Title'),'customer_id' => __('Customer'),'entity_pk_value' => __('Product')];
    }
}
