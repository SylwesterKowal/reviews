<?php
/**
 * Display All Reviews
 * Copyright (C) 2019  kowal sp. z o.o.
 *
 * This file is part of Kowal/Reviews.
 *
 * Kowal/Reviews is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Kowal\Reviews\Model\Config\Source;

class Latest implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        return [['value' => '1', 'label' => __('1')],['value' => '2', 'label' => __('2')],['value' => '3', 'label' => __('3')],['value' => '4', 'label' => __('4')]];
    }

    public function toArray()
    {
        return ['1' => __('1'),'2' => __('2'),'3' => __('3'),'4' => __('4')];
    }
}
