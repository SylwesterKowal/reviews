<?php
/**
 * Display All Reviews
 * Copyright (C) 2019  kowal sp. z o.o.
 *
 * This file is part of Kowal/Reviews.
 *
 * Kowal/Reviews is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Kowal\Reviews\Block\All;

class Index extends \Magento\Framework\View\Element\Template
{

    /**
     * Index constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     * @param \Magento\Review\Model\ResourceModel\Review\CollectionFactory $collectionFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param \Magento\Catalog\Helper\Image $imageHelper
     * @param \Kowal\Reviews\Helper\Data $dataHelper
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = [],
        \Magento\Review\Model\ResourceModel\Review\CollectionFactory $collectionFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Catalog\Helper\Image $imageHelper,
        \Kowal\Reviews\Helper\Data $dataHelper,
        \Magento\Review\Model\ReviewFactory $reviewFactory,
        \Magento\Framework\App\ResourceConnection $resourceConnection
    )
    {
        parent::__construct($context, $data);

        $this->collectionFactory = $collectionFactory;
        $this->storeManager = $storeManager;
        $this->productRepository = $productRepository;
        $this->imageHelper = $imageHelper;
        $this->dataHelper = $dataHelper;
        $this->reviewFactory = $reviewFactory;
        $this->resourceConnection = $resourceConnection;
    }

    public function getCollectionReviesLatest($limit = 3)
    {
        $limit = $limit ?? 3;
        $reviewsCollection = $this->getCollectionReviews(true);
        return $reviewsCollection->setPageSize($limit);
    }

    public function getCollectionReviesCount()
    {
        $reviewsCollection = $this->getCollectionReviews(true);
        return $reviewsCollection->count();
    }

    public function getCollectionReviews( $all = false)
    {
        $page = ($this->getRequest()->getParam('p')) ? $this->getRequest()->getParam('p') : 1;
        $pageSize=($this->getRequest()->getParam('limit'))? $this->getRequest()->getParam('limit') : 10;

        $reviewsCollection = $this->collectionFactory->create()
            ->addStoreFilter($this->storeManager->getStore()->getStoreId())
            ->addStatusFilter(\Magento\Review\Model\Review::STATUS_APPROVED)
            ->setDateOrder();
        $reviewsCollection->setOrder($this->getOrderByt(), 'ASC');
        if(!$all) {
            $reviewsCollection->setPageSize($pageSize);
            $reviewsCollection->setCurPage($page);
        }
        return $reviewsCollection;
    }

    public function getTotalAvgReview()
    {
        return $this->calculateAvg();
    }


    public function getProductById($id)
    {
        return $this->productRepository->getById($id);
    }

    public function getImage($product)
    {
        return $this->imageHelper->init($product, 'product_page_image_small')
            ->setImageFile($product->getImage()) // image,small_image,thumbnail
            ->resize(280)
            ->getUrl();
    }

    public function getReviewsLayout()
    {
        if ($uklad = $this->dataHelper->getGeneralCfg('uklad')) {
            return $uklad;
        } else {
            return 'kolumny';
        }
    }

    public function displayDate()
    {
        if ($display = $this->dataHelper->getGeneralCfg('display_date')) {
            return $display;
        } else {
            return true;
        }
    }

    public function getRatingSummary($product)
    {

        $this->reviewFactory->create()->getEntitySummary($product, $this->storeManager->getStore()->getId());
        if ($ratingSummary = $product->getRatingSummary()->getRatingSummary()) {
            return $ratingSummary;
        } else {
            return 0;
        }


    }

    private function getOrderByt()
    {
        if ($sortuj = $this->dataHelper->getGeneralCfg('sortuj')) {
            return $sortuj;
        } else {
            return 'created_at';
        }
    }

    private function calculateAvg()
    {
        $connection = $this->resourceConnection->getConnection();
        $rating_option_vote = $connection->getTableName('rating_option_vote');
        $rating = $connection->getTableName('rating');
        $review = $connection->getTableName('review');
        $query = "select rt.rating_code, 
                avg(vote.percent) as percent from " . $rating_option_vote . " as vote 
                inner join " . $rating . " as rt
                on(vote.rating_id=rt.rating_id)
                inner join " . $review . " as rr
                on(vote.entity_pk_value=rr.entity_pk_value)
                where rt.entity_id=1 and  rr.status_id = 1
                group by rt.rating_code";
        $result = $connection->fetchAll($query);
        $avg_rev = 0;
        if (is_array($result)) {
            $reviews_count = count($result);
            $total_prec = 0;
            foreach ($result as $rew) {
                $total_prec += $rew['percent'];
            }
            $percent = $total_prec / $reviews_count;
            $avg_rev = round($percent * 5 / 100, 1);

        }
        return $avg_rev;
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $this->pageConfig->getTitle()->set(__('Review Pagination'));
        if ($this->getCollectionReviews()) {
            $pager = $this->getLayout()->createBlock(
                'Magento\Theme\Block\Html\Pager',
                'custom.review.pager'
            )->setAvailableLimit([ 10 => 10, 15 => 15, 20 => 20])
                ->setShowPerPage(true)->setCollection(
                    $this->getCollectionReviews()
                );
            $this->setChild('pager', $pager);
            $this->getCollectionReviews()->load();
        }
        return $this;
    }
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }
}
